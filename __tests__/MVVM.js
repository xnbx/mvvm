/**
 * @jest-environment jsdom
 */

import MVVM from "../MVVM";

it('v-model', ()=>{
    document.body.innerHTML = '<div id="app"><input id="input" type="text" v-model="message.a"></div>'
    const vm = new MVVM({
        el:'#app',
        data:{
            message: {
                a: 'hello',
            },
            a:1
        }
    })
    vm.message.a = 1 ;
    expect(vm.message.a).toBe(1);
});

it('v-text', ()=>{
    document.body.innerHTML = '<div id="app"><input id="input" type="text" v-model="message.a">{{message.a}}</div>'
    const vm = new MVVM({
        el:'#app',
        data:{
            message: {
                a: 'hello',
            },
            a:1
        }
    })
    vm.message.a = 1 ;
    expect(vm.message.a).toBe(1);
});
