const path = require('path')
module.exports = {
    mode:"production",
    entry: path.resolve(__dirname,'./MVVM.js'),
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'MVVM.js'
    }
};